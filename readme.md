Diaspora hide people and tags
=============================

**[Lire la version française](readme-fr.md)**

## Why

Diaspora* allows you to block users, but what if they were able to comment your posts without seeing their posts? (for example: they post with a tag you follow)

Diaspora* allows you to follow tags, but what if you don't want to see posts with #diaspora tag in a language you don't understand? (for example: #diaspora and #french)
If someone post with #diaspora tag (that you follow) and #french (that you don't understand), you can hide this post.

## Install

* Install browser extension [GreaseMonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) or [Scriptish](https://addons.mozilla.org/firefox/addon/scriptish) or whatever
* Click [this link](https://framagit.org/spf/diaspora-hide-people-tags/raw/master/diaspora-hide-people-tags.user.js)
* Click ``Install``
* Access to your account

## My pod is not diaspora-fr.org nor framasphere.org

I made this script for these two pods, but it could be adapted and added to other pods. So, to install this script for **your** pod:

* Go to your browser settings (Firefox ``about:addons``)
* ``User Scripts``
* Click ``Settings`` in front of **diaspora* hide people and tags**
* Add your pod url in **Matched pages** section (Don't forget to add a *joker* at the end : ``https://my-pod-url/*``) and ``your-pod-url/people/*`` to *Excluded pages*.
* Deactivate-reactivate the script to enable changes

## How to add people or/and tags

* Go to your browser settings (Firefox ``about:addons``)
* ``User Scripts``
* Click ``Settings`` in front of **diaspora* hide people and tags**
* Click ``Edit``
* Copy diaspora's user id (right after `/people/`: 'pod-url/people/id')
* Paste id in **HideMe** array :

````
var HideMe = [
  'id',
];
````

* Save

## Example

You want to hide my posts (well, it's just an example, eh?!) and #french tag :

````
var HideMe = [
  'a67fe237dabfccac', // my id (url: https://diaspora-fr.org/people/a67fe237dabfccac)
  'french',
  'anotherID',
  'anoterTag',
];
````

**Don't forget comma at the end of each line**.

## How it works

This script checks when you scroll, and hide posts with specific tags and/or from specific people.

## FAQ (actually, no one asked)

* **Q**: Why using scroll, and not waiting page loading?
* **A**: Because diaspora\* use Ajax to load stream, so my script is loaded **before** and doesn't work.

* **Q**: Your readme is endless, is it finish soon?
* **A**: Yup.
